const isResponseOk = (response) => {
    if (response.status === 200) {
        return true
    }
    return false
}

export default isResponseOk;