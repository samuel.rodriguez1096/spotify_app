import { create } from 'apisauce';

const API = () => {
    const api = create({
        baseURL: 'https://accounts.spotify.com'
    })
    const authentincatingUser = () => {
        return api.post('/api/token',
            'grant_type=client_credentials',
            {
                // grant_type: 'client_credentials',
                headers: {
                    Authorization: 'Basic ZjdkMjAzMjg4NGUyNGQ1MmJhZjE3OTdmZDRjZmMxZTU6ODlmZDZmNWUzMTJmNDNhMmE0MTcyOGQ4NzlhODE0MDE=',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
    }
    const searchingTracks = (token,query) => {
        api.setBaseURL('https://api.spotify.com')
        api.setHeader('Authorization',`Bearer ${token}`)
        return api.get(`/v1/search?q=${query}&type=track`)
    }
    return { authentincatingUser, searchingTracks}
}


export default API;