
function adaptTrackList(rawData) {
    const theList = rawData.map((element) => {
        return adaptTrackItem(element);
    })
    return theList
}
function adaptTrackItem(rawItem) {
    const {
        name,
        id,
        artists,
        duration_ms,
        explicit,
        available_markets,
        album,
        preview_url
    } = rawItem || {};
    return {
        name,
        id,
        artist: artists ? artists[0].name : '',
        duration: duration_ms,
        explicit,
        available_markets,
        album_image: album.images ? album.images[0].url : '',
        album_name: album.name,
        album_release_date: album.release_date,
        preview_url
    }
}

export default adaptTrackList