import { all, call, put, takeLatest } from 'redux-saga/effects';
import { Creators, Types } from '../redux/index'
import API from '../services/api';
import isResponseOk from '../services/utils';
import adaptTrackList from '../services/TracksAdapter';
const theApi = API();

function* fetchingToken(api, action) {
    yield put(Creators.theRequest())
    const response = yield call(api.authentincatingUser)
    if (isResponseOk(response)) {
        yield put(Creators.loggingUserSuccessful(response.data.access_token, true, false))
        const tracksResponse = yield call(api.searchingTracks, response.data.access_token, action.query)
        if (isResponseOk(tracksResponse)) {
            console.log('response',tracksResponse)
            yield put(Creators.tracksListSuccessful(adaptTrackList(tracksResponse.data.tracks.items), true, false))
        } else {
            yield put(Creators.tracksListError(null, false, true))
        }
    } else {
        yield put(Creators.loggingUserFailed(null, false, true))
    }

}


function* root() {
    yield all([takeLatest(Types.LOGGING_USER, fetchingToken, theApi)])
}

export default root;