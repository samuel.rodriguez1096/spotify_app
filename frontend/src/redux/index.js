import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import root from '../saga/index';
const INITIAL_STATE = Immutable({
    previous: null,
    next: null,
    loading: false,
    tracks: null,
    trackSelected: {},
    isTrackSelected: false,
    limit: 20,
    total: 0,
    token: null,
    errorLogin: false,
    succesfulLogin: false,
    errorTracks: false,
    successfulTracks: false
})
const sagaMiddleware = createSagaMiddleware()

//Reducer functions here

const request = (state, action) => {
    return state.merge({
        loading: true
    })
}

const loggedSuccessful = (state, action) => {
    return state.merge({
        token: action.token,
        succesfulLogin: action.successfulLogin,
        errorLogin: action.errorLogin,
    })
}
const loggedFailed = (state, action) => {
    return state.merge({
        errorLogin: action.errorLogin,
        succesfulLogin: action.succesfulLogin,
        token: action.token,

    })
}

const tracksSuccessful = (state, action) => {
    const { tracks, successfulTracks, errorTracks } = action;
    return state.merge({
        tracks,
        successfulTracks,
        errorTracks,
        loading: false
    })
}

const tracksFailed = (state, action) => {
    const { tracks, successfulTracks, errorTracks } = action;
    return state.merge({
        tracks,
        successfulTracks,
        errorTracks,
        loading: false
    })
}

export const { Types, Creators } = createActions({
    loggingUser: ['query'],
    loggingUserSuccessful: ['token', 'successfulLogin', 'errorLogin'],
    loggingUserFailed: ['token', 'successfulLogin', 'errorLogin'],
    tracksListSuccessful: ['tracks', 'successfulTracks', 'errorTracks'],
    tracksListError: ['tracks', 'successfulTracks', 'errorTracks'],
    theRequest: []

})

const HANDLERS = {
    [Types.LOGGING_USER_SUCCESSFUL]: loggedSuccessful,
    [Types.LOGGING_USER_FAILED]: loggedFailed,
    [Types.TRACKS_LIST_SUCCESSFUL]: tracksSuccessful,
    [Types.TRACKS_LIST_ERROR]: tracksFailed,
    [Types.THE_REQUEST]: request,
}

let myReducer = createReducer(INITIAL_STATE, HANDLERS);
let store = createStore(myReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(root)
export default store