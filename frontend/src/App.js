import React from 'react';
import { connect } from 'react-redux';
import { Creators } from './redux/index';
import { Formik } from 'formik';
import './styles/styles.css'
class App extends React.Component {
  theBody = () => {
    if(this.props.loading) {
      //spinner
      return (<p>cargando</p>)
    } else if (!this.props.loading && this.props.tracks != null) {
      return (<p>hay tracks</p>)
    } else {
      return null
    }
  }
  TheForm = () => (
    <div className='container'>
      <div className='row mt-5'>
        <div className='col'></div>
        <div className='col'>
          <Formik
            initialValues={{ query: '' }}
            validate={values => {
              const errors = {}
              if (!values.query) {
                errors.query = 'Required'
              }
              return errors
            }}
            onSubmit={(values, { setSubmitting }) => {
              // console.log('fromFormil', values, setSubmitting)
              this.props.loggingUser(values.query)
              setSubmitting(false)
            }}
          >
            {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <form onSubmit={handleSubmit} className='form-inline'>
                <div className='form-group'>
                  <input
                    type='text'
                    name='query'
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.query}
                    className='form-control'
                    id="formGroupExampleInput"
                    placeholder="Track To Search"
                  />

                  <button type="submit" className='btn btn-dark ml-2' disabled={isSubmitting}>
                    Submit
                  </button>
                </div>
                {errors.query && touched.query && errors.query}
              </form>
            )}
          </Formik>
        </div>
        <div className='col'></div>
      </div>
    </div>
  )


  RenderingIndex = () => (
    <div>
      <header>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <img src='images/spotifyv1.png' alt='here' className='MyClass'></img>
          <a className='navbar-brand' href='/'>Spotify App</a>
        </nav>
      </header>
    </div>
  )


  render() {
    if (!this.props.isTrackSelected) {
      return (
        <section>
          { this.RenderingIndex()}
          {this.TheForm()}
          {this.theBody()}
        </section>
      )
    }
    return null
  }
}

const mapStateToProps = state => ({
  loggedUser: state.succesfulLogin,
  isThereTracks: state.successfulTracks,
  userToken: state.token,
  tracks: state.tracks,
  isTrackSelected: state.isTrackSelected,
  loading: state.loading
})
const mapDispatchToProps = dispatch => ({
  loggingUser: (query) => dispatch(Creators.loggingUser(query))
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
