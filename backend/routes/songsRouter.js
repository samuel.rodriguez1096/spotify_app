const router = require('express').Router();
const axios = require('axios').default;

const gettingToken = () => {
    return axios.post('https://accounts.spotify.com/api/token', 'grant_type=client_credentials', {
        headers: {
            Authorization: 'Basic ZjdkMjAzMjg4NGUyNGQ1MmJhZjE3OTdmZDRjZmMxZTU6ODlmZDZmNWUzMTJmNDNhMmE0MTcyOGQ4NzlhODE0MDE=',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}

router.get('/', async (req, res) => {
    gettingToken().then((result) => {
        console.log('successful', result.data.access_token)
    }).catch((error) => {
        console.log('error', Object.keys(error), error.request)
    })
    res.status(200).send('good')
})


module.exports = {
    router
}