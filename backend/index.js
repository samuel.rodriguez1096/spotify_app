const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");



const server = express();
const cors = require('cors')

server.use(cors());
server.use(express.json());

const songsRouter = require('./routes/songsRouter').router;
server.use('/api/v1/spotify-app/songs', songsRouter);

http.createServer(server).listen(5500, () => {
    console.log('Backend Service initialized successfully')
})